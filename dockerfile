FROM registry.gitlab.com/darksidevt-containers/python-docker:latest

COPY main.py requirements.txt .env /data/
RUN pip install -r /data/requirements.txt
ENTRYPOINT ["python", "/data/main.py"]