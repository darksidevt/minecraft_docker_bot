import discord
import docker
from rcon.source import Client
import re
from discord.ext import tasks, commands
from dotenv import load_dotenv
import os

load_dotenv()

'''
Environment Variables
'''
CONTAINERNAME:str = os.getenv("CONTAINERNAME")
TESTGUILDS:str = os.getenv("TESTGUILDS")
RCONPASSWORD:str = os.getenv("RCONPASSWORD")
BOT_TOKEN:str = os.getenv("BOT_TOKEN")

docker_client = docker.from_env()
intents = discord.Intents.all()
intents.message_content = True
guild_ids = TESTGUILDS.split(",")
bot = discord.Bot(intents=intents, debug_guilds=guild_ids)

# Function to check if the server is running
def check_if_server_is_running():
    containers = docker_client.containers.list()
    if CONTAINERNAME in [container.name for container in containers]:
        return True
    else:
        return False

# Function to check how many players are online and who they are
def players_online():
    with Client('10.0.1.52', 25575, passwd=RCONPASSWORD) as client:
        response = client.run('/list')
    # Example response:
    # "There are 1 of a max of 20 players online: DarksideVT, nnuasabuvWEFG, whinabego"
    #Extract the number of players online
    reg = '[0-9]+'
    num_players = re.findall(reg, response)
    # Get a list with names of the players online
    reg = '(?<=: ).*'
    string_players = re.findall(reg, response)
    # Split the string into a list of players
    players = string_players[0].split(", ")
    return num_players[0],num_players[1], players
# Print the bot has started
@bot.event
async def on_ready():
    print(bot.user.name, "has connected to Discord!")
    
    

# Bot command to stop the minecraft server
@bot.slash_command(name="stop", description="Stop the minecraft server")
async def stop(ctx):
    await ctx.respond("Stopping the minecraft server")
    containers = docker_client.containers.list()
    if CONTAINERNAME in [container.name for container in containers]:
        container = docker_client.containers.get(CONTAINERNAME)
        container.stop()
        await ctx.respond("Minecraft server stopped", ephemeral=True)
    else:
        await ctx.respond("Minecraft server is not running")
        
        
#Bot command to check how many players are online
@bot.slash_command(name="players", description="Check how many players are online")
async def players(ctx):
    await ctx.respond("Checking how many players are online")
    if not check_if_server_is_running():
        await ctx.respond("Minecraft server is not running", ephemeral=True)
        return
    num_players, max_players, players = players_online()
    await ctx.respond(f"There are {num_players} of a max of {max_players} players online: {', '.join(players)}")
    
#Bot command to start the minecraft server
@bot.slash_command(name="start", description="Start the minecraft server")
async def start(ctx):
    await ctx.respond("Starting the minecraft server")
    containers = docker_client.containers.list()
    if CONTAINERNAME in [container.name for container in containers]:
        await ctx.respond("Minecraft server is already running", ephemeral=True)
    else:
        container = docker_client.containers.get(CONTAINERNAME)
        container.start()
        auto_stop.start()
        await ctx.respond("Minecraft server started", ephemeral=True)

#Bot command to restart the minecraft server
@bot.slash_command(name="restart", description="Restart the minecraft server")
async def restart(ctx):
    await ctx.respond("Restarting the minecraft server")
    containers = docker_client.containers.list()
    if CONTAINERNAME in [container.name for container in containers]:
        container = docker_client.containers.get(CONTAINERNAME)
        container.restart()
        await ctx.respond("Minecraft server restarted", ephemeral=True)
    else:
        await ctx.respond("Minecraft server is not running", ephemeral=True)

#Bot command to check the status of the minecraft server
@bot.slash_command(name="status", description="Check the status of the minecraft server")
async def status(ctx):
    await ctx.respond("Checking the status of the minecraft server")
    containers = docker_client.containers.list()
    if CONTAINERNAME in [container.name for container in containers]:
        container = docker_client.containers.get(CONTAINERNAME)
        status = container.status
        await ctx.respond(f"Minecraft server is {status}", ephemeral=True)
    else:
        await ctx.respond("Minecraft server is not running", ephemeral=True)

@tasks.loop(seconds=10)
async def loop_start():
    containers = docker_client.containers.list()
    if CONTAINERNAME in [container.name for container in containers]:
        num_players, max_players, players = players_online()
        if num_players == "0":
            auto_stop.start()
            print("Minecraft server started")
        else:
            print("Players online, not stopping the timer.")
            auto_stop.stop()
    else:
        print("Minecraft server is not running")

#Every 30 minutes check if the server is running and if there are no players online, stop the server
@tasks.loop(minutes=30)
async def auto_stop():
    containers = docker_client.containers.list()
    if CONTAINERNAME in [container.name for container in containers]:
        num_players, max_players, players = players_online()
        if num_players == "0":
            container = docker_client.containers.get(CONTAINERNAME)
            container.stop()
            auto_stop.stop()
            print("Minecraft server stopped")
        else:
            print("Minecraft server is running and there are players online")
    else:
        print("Minecraft server is not running")
        
        
bot.run(BOT_TOKEN)